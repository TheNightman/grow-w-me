import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:grow_w_me/user_repo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository _userRepository;
  final Firestore _firestore;

  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        _firestore = Firestore.instance,
        super(AuthenticationInitial());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AuthenticationStarted)
      yield* _mapAuthenticationStartedToState();
    else if (event is AuthenticationLoggedIn)
      yield* _mapAuthenticationLoggedInToState();
    else if (event is AuthenticationLoggedOut)
      yield* _mapAuthenticationLoggedOutToState();
  }

  Stream<AuthenticationState> _mapAuthenticationStartedToState() async* {
    final isSignedIn = await _userRepository.isSignedIn();
    if (isSignedIn) {
      final user = await _userRepository.getUser();
      yield AuthenticationSuccess(user);
    } else {
      yield AuthenticationFailure();
    }
  }

  Stream<AuthenticationState> _mapAuthenticationLoggedInToState() async* {
    CollectionReference users = _firestore.collection('users');
    FirebaseUser user = await _userRepository.getUser();
    DocumentSnapshot userData = await users.document(user.uid).get();
    if (!userData.exists) {
      users
          .document(user.uid)
          .setData(
              {'username': user.displayName, 'email': user.email, 'media': []})
          .then((value) => print("user added"))
          .catchError((err) => print("Failed to add user: $err"));
    }
    yield AuthenticationSuccess(user);
  }

  Stream<AuthenticationState> _mapAuthenticationLoggedOutToState() async* {
    yield AuthenticationFailure();
    _userRepository.signOut();
  }
}
