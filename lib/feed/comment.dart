import 'package:equatable/equatable.dart';

class Comment extends Equatable {
  final String id;
  final String author;
  final String content;
  final int score;

  const Comment({this.id, this.author, this.content, this.score});

  @override
  List<Object> get props => [id, author, content, score];

  @override
  String toString() => 'Comment { id: $id }';
}
