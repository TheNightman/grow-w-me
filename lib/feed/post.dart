import 'package:equatable/equatable.dart';
import 'package:grow_w_me/feed/comment.dart';

class Post extends Equatable {
  final int id;
  final String title;
  final String author;
  final String body;
  final int score;
  final List<String> tags;
  final List<String> attachments;
  final List<Comment> comments;

  const Post(
      {this.id,
      this.title,
      this.author,
      this.body,
      this.score,
      this.tags,
      this.attachments,
      this.comments});

  @override
  List<Object> get props =>
      [id, title, author, body, score, tags, attachments, comments];

  @override
  String toString() => 'Post { id: $id }';
}
