import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:grow_w_me/feed/post.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

part 'feed_event.dart';
part 'feed_state.dart';

class FeedBloc extends Bloc<FeedEvent, FeedState> {
  final Firestore firestore;

  FeedBloc({@required this.firestore}) : super(FeedInitial());

  @override
  Stream<FeedState> mapEventToState(
    FeedEvent event,
  ) async* {
    final currentState = state;
    if (event is FeedPostFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is FeedInitial) {
          final posts = await _fetchPosts(0, 20);
          yield FeedPostSuccess(posts: posts, hasReachedMax: false);
          return;
        }
        if (currentState is FeedPostSuccess) {
          final posts = await _fetchPosts(currentState.posts.length, 20);
          yield posts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : FeedPostSuccess(
                  posts: currentState.posts + posts,
                  hasReachedMax: false,
                );
        }
      } catch (_) {
        yield FeedPostFailure();
      }
    }
  }

  @override
  Stream<Transition<FeedEvent, FeedState>> transformEvents(
    Stream<FeedEvent> events,
    TransitionFunction<FeedEvent, FeedState> transitionFunction,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFunction,
    );
  }

  bool _hasReachedMax(FeedState state) =>
      state is FeedPostSuccess && state.hasReachedMax;

  Future<List<Post>> _fetchPosts(int startIndex, int limit) async {
    QuerySnapshot snapshot = await firestore.collection('posts').getDocuments();
    var postSnapshots = snapshot.documents.sublist(startIndex, limit);
    if (postSnapshots.isNotEmpty) {
      return postSnapshots.map((post) {
        return Post(
          title: post["Title"],
          author: post["Author"],
          body: post["Body"],
          score: post["Score"],
          attachments: post["Attachments"],
          comments: post["Comments"],
          tags: post["Tags"],
        );
      }).toList();
    } else {
      throw Exception('Error fetching posts');
    }
  }
}
