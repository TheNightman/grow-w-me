part of 'feed_bloc.dart';

abstract class FeedState extends Equatable {
  const FeedState();

  @override
  List<Object> get props => [];
}

class FeedInitial extends FeedState {}

class FeedPostFailure extends FeedState {}

class FeedPostSuccess extends FeedState {
  final List<Post> posts;
  final bool hasReachedMax;

  const FeedPostSuccess({this.posts, this.hasReachedMax});

  FeedPostSuccess copyWith({
    List<Post> posts,
    bool hasReachedMax,
  }) {
    return FeedPostSuccess(
      posts: posts ?? this.posts,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  List<Object> get props => [posts, hasReachedMax];

  @override
  String toString() =>
      'FeedPostSuccess { posts: ${posts.length}, hasReachedMax: $hasReachedMax }';
}
