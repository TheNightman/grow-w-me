part of 'feed_bloc.dart';

abstract class FeedEvent extends Equatable {
  const FeedEvent();

  @override
  List<Object> get props => [];
}

class FeedRefresh extends FeedEvent {}

class FeedPostFetched extends FeedEvent {}

class FeedPostCreate extends FeedEvent {}

class FeedPostVote extends FeedEvent {
  final String postId;

  const FeedPostVote({@required this.postId});

  @override
  List<Object> get props => [];

  @override
  String toString() => 'FeedPostVote { postId: $postId }';
}

class FeedPostSelect extends FeedEvent {
  final String postId;

  const FeedPostSelect({@required this.postId});

  @override
  List<Object> get props => [postId];

  @override
  String toString() => 'FeedPostSelect { postId: $postId }';
}
