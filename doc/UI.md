          App
Feed  -  Posts  -  Account
Appbar   Appbar    Appbar 
ListItem ListItem  InfoItems

When a user makes a post from the feed view, the app state is notified and the posts page will be updated.

When a user subscribes to a user, the app state is updated and the account page will be updated, along with the feed depending on how the feed is being sorted

When a user votes on a post, the app state is notified and the post view will be updated


### Feed
shows Post previews
each preview shows the title, an excerpt from the post (summary), the author, some tags, and the post score